import os

pybabel = 'pybabel'
os.system(pybabel +
          ' extract -F Web/babel.cfg -k lazy_gettext -o Web/messages.pot ' +
          'Web/app')
os.system(pybabel +
          ' update -i Web/messages.pot -d Web/app/translations')
os.unlink('messages.pot')
