import os
import sys

pybabel = 'pybabel'
if len(sys.argv) != 2:
    print "usage: tr_init <language-code>"
    sys.exit(1)
os.system(pybabel +
          ' extract -F Web/babel.cfg -k lazy_gettext -o Web/messages.pot ' +
          'Web/app')
os.system(pybabel +
          ' init -i Web/messages.pot -d Web/app/translations -l ' +
          sys.argv[1])
os.unlink('Web/messages.pot')
