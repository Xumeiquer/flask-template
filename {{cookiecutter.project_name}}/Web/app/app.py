# -*- coding: utf-8 -*-
from __future__ import print_function, absolute_import

from flask import Flask, render_template, request

from .settings import ProdConfig
from .extensions import (
    babel,
    db,
    migrate,
)
from .api.v1.view import IndexView
from .page.view import bp as index


def create_app(config_object=ProdConfig):
    '''An application factory, as explained here:
       http://flask.pocoo.org/docs/patterns/appfactories/
       :param config_object: The configuration object to use.
    '''
    app = Flask(__name__)
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    register_errorhandlers(app)
    return app


def register_extensions(app):
    babel.init_app(app)
    configure_babel(app)
    db.init_app(app)
    migrate.init_app(app, db)
    return None


def register_blueprints(app):
    app.register_blueprint(index)
    IndexView.register(app)
    return None


def register_errorhandlers(app):
    def render_error(error):
        # If a HTTPException, pull the `code` attribute; default to 500
        error_code = getattr(error, 'code', 500)
        return render_template("errors/{0}.html".format(error_code)),\
            error_code
    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)
    return None


def register_before_request(app):
    return None


def register_after_request(app):
    return None


def configure_babel(app):
    @babel.localeselector
    def get_locale():
        languages = app.config['LANGUAGES']
        return request.accept_languages.best_match(languages.keys())
