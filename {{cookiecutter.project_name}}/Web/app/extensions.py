# -*- coding: utf-8 -*-
"""Extensions module. Each extension is initialized in the app factory located
in app.py
"""
from __future__ import print_function, absolute_import


from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.migrate import Migrate
from flask.ext.babel import Babel

db = SQLAlchemy()
migrate = Migrate()
babel = Babel()
