# -*- coding: utf-8 -*-
from __future__ import absolute_import

from flask import Blueprint, render_template
from flask.ext.babel import gettext

bp = Blueprint('index', __name__, url_prefix='/')


@bp.route('/', methods=['GET'])
def index():
    return render_template('index/index.html')
